package com.pop.app.hansab;

import com.pop.app.hansab.mails.Email;
import com.pop.app.hansab.mails.MailRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class HansabApplicationTests {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private MailRepository mailRepository;

	@Test
	public void whenFindBySubject_thenReturnMail() {
		Email mail= new Email("raza.rana@hotmail.com","Testing","Hello, Hope you are doing fine");
		entityManager.persist(mail);
		entityManager.flush();

		Email found = mailRepository.findEmailByMailSubject("Testing");
		assertThat(found.getMailSubject()).isEqualTo(mail.getMailSubject());
	}

	@Test
	public void whenSubjectContains3001_thenReturnColorRed() {
		Email mail= new Email("raza.rana@hotmail.com","Testing3001","Hello, Hope you are doing fine");

		assertThat(mail.getCode()).isEqualTo("red");
	}

	@Test
	public void whenBodyContains3001_thenReturnColorRed() {
		Email mail= new Email("raza.rana@hotmail.com","TestingBody","Hello, Hope you are doing fine. We have code 3001 problem please report.");

		assertThat(mail.getCode()).isEqualTo("red");
	}

	@Test
	public void whenSubjectContains2001_thenReturnColorYellow() {
		Email mail= new Email("raza.rana@hotmail.com","Testing2001","Hello, Hope you are doing fine");

		assertThat(mail.getCode()).isEqualTo("yellow");
	}

	@Test
	public void whenBodyContains2001_thenReturnColorYellow() {
		Email mail= new Email("raza.rana@hotmail.com","TestingBody","Hello, Hope you are doing fine. We have code 2001 problem please report.");

		assertThat(mail.getCode()).isEqualTo("yellow");
	}

	@Test
	public void whenSubjectContains1001_thenReturnColorGreen() {
		Email mail= new Email("raza.rana@hotmail.com","Testing1001","Hello, Hope you are doing fine");

		assertThat(mail.getCode()).isEqualTo("green");
	}

	@Test
	public void whenBodyContains1001_thenReturnColorGreen() {
		Email mail= new Email("raza.rana@hotmail.com","TestingBody","Hello, Hope you are doing fine. We have code 1001 problem please report.");

		assertThat(mail.getCode()).isEqualTo("green");
	}

	@Test
	public void whenSubjectAndBodyContainsNoCode_thenReturnColorGreen() {
		Email mail= new Email("raza.rana@hotmail.com","Testing","Hello, Hope you are doing fine");

		assertThat(mail.getCode()).isEqualTo("green");
	}



}
