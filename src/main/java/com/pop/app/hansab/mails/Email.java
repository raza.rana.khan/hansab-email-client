package com.pop.app.hansab.mails;
import javax.persistence.*;

@Entity
public class Email {



    //AutoIncrementing email id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "mail_id", updatable = true, nullable = false)
    private Long mailId;
    private String mailSender;
    private String mailSubject;
    //limiting mail body length to 10000
    @Column(name="mailBody", length=10000)
    private String mailBody;
    private String mailCode;


    //default constructor
    public Email() {
    }


    //constructor to construct an email
    public Email(String mailSender, String mailSubject, String mailBody) {
        this.mailSender = mailSender;
        this.mailSubject = mailSubject;
        this.mailBody = mailBody;

        //assign the code to the mail on the basis of code
        if(mailBody.contains("3001") || mailSubject.contains("3001")){
            this.mailCode="red";
        }
        else if(mailBody.contains("2001") || mailSubject.contains("2001")){
            this.mailCode="yellow";
        }
        else {
            this.mailCode="green";
        }

    }

    //mail entity getters setters
    public Long getMailId() {
        return mailId;
    }

    public void setMailId(Long mailId) {
        this.mailId = mailId;
    }

    public String getCode() {
        return mailCode;
    }

    public void setCode(String code) {
        this.mailCode = code;
    }

    public String getMailSender() {
        return mailSender;
    }

    public void setMailSender(String mailSender) {
        this.mailSender = mailSender;
    }

    public String getMailSubject() {
        return mailSubject;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }

    public String getMailBody() {
        return mailBody;
    }

    public void setMailBody(String mailBody) {
        this.mailBody = mailBody;
    }



}
