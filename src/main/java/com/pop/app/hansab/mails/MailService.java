package com.pop.app.hansab.mails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.mail.MailHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;


//This class is responsible for compiling and maintaining mail list
@Component
public class MailService {


    @Autowired
    private MailRepository mailRepository;
    private List<Email> emailList =new ArrayList<Email>();



    public List<Email> getEmailList() {
        //clear the old list of emails
        emailList.clear();
        //get fresh list of emails from database
        mailRepository.findAll().forEach(emailList::add);
        return emailList;
    }

    public Message addToList(Message message)throws Exception{

        //get headers from the message
        MessageHeaders headers = message.getHeaders();
        String mailSender=headers.get(MailHeaders.FROM).toString();
        String mailSubject=headers.get(MailHeaders.SUBJECT).toString();

        //get mail from message payload and convert to string
        String mailBody=new String((byte [])message.getPayload());

        //save the new email in the database
        mailRepository.save(new Email(mailSender,mailSubject, removeHeaders(mailBody)));

        return message;
    }



    String removeHeaders(String body){

        //first line contain encryption code
        String messageCode=body.split(System.getProperty("line.separator"))[0];

        //get the text part of body by splitting body on the bases of code an then remove header
        String textBody=body.split(messageCode)[1].replace("Content-Type: text/plain; charset=\"UTF-8\"","");

        //trim to String
        if(textBody.length()>9999){
            return textBody.substring(0,9999);
        }


        return textBody;

    }

}
