package com.pop.app.hansab.mails;


import org.springframework.integration.mail.AbstractMailReceiver;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.MimeMessage;

@Service
public class Pop3MailReceiver extends AbstractMailReceiver {

    public Pop3MailReceiver()throws MessagingException {
        super(new URLName("pop3", "pop.gmail.com", 995, "INBOX", "tester12048@gmail.com", "qwe123poi"));

    }

    @Override
    protected Message[] searchForNewMessages() throws MessagingException {
        Folder folderToUse = getFolder();
        int messageCount = folderToUse.getMessageCount();
        if (messageCount == 0) {
            return new Message[0];
        }
        return folderToUse.getMessages();
    }

    @Override
    protected void deleteMessages(Message[] messages) throws     MessagingException {
        super.deleteMessages(messages);


        for (int i = 0; i < messages.length; i++) {

            new MimeMessage((MimeMessage) messages[i]);


        }


    }
}
