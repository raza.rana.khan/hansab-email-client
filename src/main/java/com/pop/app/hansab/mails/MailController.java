package com.pop.app.hansab.mails;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class MailController {



    @Autowired
    private MailService mailList;


    //redirecting to inbox
    @RequestMapping("/")
    public String redirect(Model model){
        return "redirect:inbox";
    }

    //inbox
    @RequestMapping("/inbox")
    public String getMails(Model model){
        model.addAttribute("Emails", mailList.getEmailList());
        return "inbox";
    }





}
