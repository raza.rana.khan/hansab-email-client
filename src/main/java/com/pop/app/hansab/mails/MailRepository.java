package com.pop.app.hansab.mails;


import org.springframework.data.repository.CrudRepository;

//Mail Repository Interface
public interface MailRepository extends CrudRepository<Email,Long> {


    public Email findEmailByMailSubject(String mail_subject);

}
