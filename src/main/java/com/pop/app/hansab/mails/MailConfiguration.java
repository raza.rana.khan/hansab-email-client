package com.pop.app.hansab.mails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.mail.MailHeaders;
import org.springframework.integration.mail.MailReceivingMessageSource;
import org.springframework.integration.mail.support.DefaultMailHeaderMapper;
import org.springframework.integration.mapping.HeaderMapper;

import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Configuration
@EnableIntegration
public class MailConfiguration {



    @Autowired
    private MailService MailList;

    @Autowired
    com.pop.app.hansab.mails.Pop3MailReceiver pop3Mail;

    //reading the mails on pop3 channel
    @Bean
    public IntegrationFlow readEmailsFromSource(){

        return IntegrationFlows.from(configurePop3Source(), e -> e.autoStartup(true).poller(p -> p.fixedDelay(1000)))
                .enrichHeaders(s -> s.headerExpressions(c -> c.put(MailHeaders.SUBJECT, "payload.subject").put(MailHeaders.FROM, "payload.from[0].toString()")))
                .transform(MailList,"addToList")
                .handle(System.out::println)
                .get();
    }



    @Bean
    public HeaderMapper<MimeMessage> mailHeaderMapper() {
        return new DefaultMailHeaderMapper();
    }

    //configuring pop3 with pop3Mail bean
    @Bean
    MailReceivingMessageSource configurePop3Source(){

        Properties properties = new Properties();
        properties.put("mail.debug", "true");
        properties.put("mail.pop3.ssl.enable", "true");


        pop3Mail.setJavaMailProperties(properties);
        pop3Mail.setShouldDeleteMessages(true);
        pop3Mail.setHeaderMapper(mailHeaderMapper());


        return  new MailReceivingMessageSource(pop3Mail);
    }
}

