package com.pop.app.hansab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class HansabApplication {


	public static void main(String[] args)throws Exception {

		SpringApplication.run(HansabApplication.class, args);


	}

}
